## Installation

1. git clone git@gitlab.com:Biboswan/flightdelayprediction.git

2. cd flightdelayprediction/

3. pip install virtualenv

4. virtualenv venv

5. source venv/bin/activate

6. pip install -r requirements.txt

7. python airline.py

8. App running on localhost:5000
